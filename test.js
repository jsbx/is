'use strict';
const is = require('./');
const assert = require('chai').assert;

//* ========================= ALL =========================
describe('all', function () {

    it(`is.primitive(undefined)`, function () {
        let actual = is.primitive(undefined);
        assert.equal(actual, true);
    });

    it(`is.undef(void 0)`, function () {
        let actual = is.undef(void 0);
        assert.equal(actual, true);
    });

    it(`is.nil(null)`, function () {
        let actual = is.nil(null);
        assert.equal(actual, true);
    });

    it(`is.bool(false)`, function () {
        let actual = is.bool(false);
        assert.equal(actual, true);
    });

    it(`is.sym(Symbol('test'))`, function () {
        let actual = is.sym(Symbol('test'));
        assert.equal(actual, true);
    });

    it(`is.str("false")`, function () {
        let actual = is.str("false");
        assert.equal(actual, true);
    });

    it(`is.emptyString("")`, function () {
        let actual = is.emptyString("");
        assert.equal(actual, true);
    });

    it(`is.alpha("abcd_")`, function () {
        let actual = is.alpha("abcd_");
        assert.equal(actual, true);
    });

}); // end describe



//* ========================= undefined =========================
describe('undefined', function () {

    it(`is.undef(): declared, but uninitilized`, function () {
        let actual;
        assert.equal(is.undef(actual), true);
    });

    it(`is.undef()`, function () {
        assert.equal(is.undef(), true);
    });

    it(`is.undef(undefined)`, function () {
        let actual = undefined;
        assert.equal(is.void(actual), true);
    });

    it(`is.undef("undefined")`, function () {
        let actual = "undefined";
        assert.equal(is.u(actual), false);
    });

    it(`is.undef(void 0)`, function () {
        let actual = void 0;
        assert.equal(is.void(actual), true);
    });

    it(`is.undef(null)`, function () {
        let actual = null;
        assert.equal(is.u(actual), false);
    });

}); // end describe


//* =========================== arrays ==========================
describe('arrays', function () {

    it(`is.array`, function () {
        let actual = [1,2,3];
        assert.equal(is.array(actual), true);
    });

    it(`is.array`, function () {
        let actual = "123";
        assert.equal(is.array(actual), false);
    });

    it(`is.arrayLike`, function () {
        assert.equal(is.arrayLike(arguments), true);
    });

    it(`is.nestedArray`, function () {
        let actual = [1, 2, 3];
        assert.equal(is.nestedArray(actual), false);
    });

    it(`is.nestedArray`, function () {
        let actual = [1, [2, 3]];
        assert.equal(is.nestedArray(actual), true);
    });


}); // end describe


//* =========================== PRIMITIVES ==========================
describe(`is.primitive(undefined)`, function () {
    
    it(`undefined`, function () {
        let actual = is.primitive(undefined);
        assert.equal(actual, true);
    });

    it(`is.primitive("undefined")`, function () {
        let actual = is.string("undefined");
        assert.equal(actual, true);
    });

});



/*
var samples = [
    {
        id: "string",
        actual: "abcd",
        expected: "abcd",
    },
    {
        id: "pojo",
        actual: { a: 1 },
        expected: { a: 1 },
    },
    {
        id: "empty pojo",
        actual: {},
        expected: {},
    },
];


describe('is', function () {
    samples.forEach(function (test) {
        it(`${test.id}`, function () {
            let actual = is.undef(test.actual());
            //let expected = test.expected;
            assert.equal(actual, false);
        });
    });
});

*/